(function ($) {
  
  $(document).ready(function() {
    $a = $('.uxbar li a', window.parent.document) || $('.uxbar li a');
    $a.click(function(e) {
      if(e.metaKey || e.ctrlKey) {
        e.preventDefault();
        var url = $(this).attr('href');
        url = url.replace('/node/add/', 'admin/structure/types/manage/');

        var overlay = "/";
        if($('#overlay').length > 0) {
          overlay = "#overlay=";
        }
        window.location.href = overlay + url;
      }
    });

    $cache = $('#toolbar-home a', window.parent.document) || $('#toolbar-home a');
    $cache.click(function(e) {
      if(e.metaKey || e.ctrlKey) {
        e.preventDefault();
        $.ajax({
          url: "/quickflush",
          type: "POST",
          success: function(data) {
            alert("Cache flushed");
          }
        });
      }
    });

    if($('.page-admin-content').length > 0) {
      $('fieldset.collapsible').addClass('collapsed');
      $('#block-system-main').prepend('<div class="sort_advanced">Meer opties</div>');

      $('.sort_advanced').click(function() {
        $('.views-field-language, .views-field-status, .views-field-changed').toggle();
      });
    }
  });

})(jQuery);
