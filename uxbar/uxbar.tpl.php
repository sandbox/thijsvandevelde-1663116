<?php

/**
 * @file
 * Default template for admin toolbar.
 *
 * Available variables:
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default value has the following:
 *   - toolbar: The current template type, i.e., "theming hook".
 * - $toolbar['toolbar_user']: User account / logout links.
 * - $toolbar['toolbar_menu']: Top level management menu links.
 * - $toolbar['toolbar_drawer']: A place for extended toolbar content.
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_toolbar()
 */

global $base_url;
?>

<a href="<?php print $base_url; ?>/node/add"><?php echo t("+ New"); ?></a>
<ul class="submenu">
<?php foreach($uxbar as $link){
  if(user_access('create ' . strtolower($link->type) . ' content')) {
?>
<li><a href="/node/add/<?php echo str_replace('_','-',$link->type);?>"><?php echo $link->name;?></a></li>
<?php }} ?>
</ul>
