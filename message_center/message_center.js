(function ($) {

var rel, id, ids, url, msg;

$(document).ready(function() {

  rel = $(".unread_msg", window.parent.document).attr('rel') || $(".unread_msg").attr('rel');
  id  = $(".unread_msg", window.parent.document).attr('rel') || $(".unread_msg").attr('rel');
  ids = id.split(','),
  msg = 0,
  url = window.location.href;
  url = url.split('/');
  msg = url[url.length - 2];

  var setRead = function() {

    //Set unread items white
    $('.sticky-table').addClass('webform_improved');
    if(ids[0] != "") {
      for(var i = 0; i < ids.length; i++) {
        $('.sticky-table')
            .find('td.active:contains(' + ids[i] + ')')
            .addClass('wf_unread')
            .parent().addClass('wf_unread')
      }
    }

    $('.webform_improved tr').find('td:eq(4)').attr('class','view_link');

    $('.sticky-table tr').each(function() {
      $(this).find('td:eq(4)').html($(this).find('td:eq(4)').html() + '<span class="mark" title="Maak ongelezen">mark unread</span>');
    });

    $('.wf_unread').each(function() {
      $(this).find('td:eq(4) .mark').remove();
      $(this).find('td:eq(4)').html($(this).find('td:eq(4)').html() + '<span class="label">unread</span>');
    });

    //Add click events to 'read' buttons
    $('.sticky-table').on('click', '.wf_unread', function(e) {
      var $frame = $(".unread_msg", window.parent.document) || $(".unread_msg");
      var $badge = $frame.find('.badge');
      var url = $(this).find('.view_link a').attr('href');
          url = url.split('/');
      var read = url[4] + '|' + url[2];
          rel = $frame.attr('rel');
          rel = rel.replace(url[4] + '|' + url[2], '');
          rel = rel.replace(',,',',');

      $thing = $(".unread_msg .count", window.parent.document) || $(".unread_msg .count");
      $frame.find('.count').text(parseInt($thing.text()) - 1);
      $frame.attr('rel', rel)

      if($frame.find('.unread_forms a').attr('href').indexOf(url[2]) != -1) {
        var $badge = $frame.find('a[rel=' + url[2] + ']').find('.badge');
        $badge.text(parseInt($badge.text()) - 1);
        if($badge.text() == 0) {
          $badge.parent().remove();
        }
      }

      if($('.count', window.parent.document).text() == '0') {
        $frame.addClass('no_unread');
      }
      
    });

    // Mark as unread
    $('.sticky-table').on('click', '.mark', function(e) {
      e.stopPropagation();

      var nid = $(this).prev('a').attr('href').split('/');
      var sid = $(this).parent().parent().addClass('wf_unread').find('.active').text();

      $('<span class="label">unread</span>').insertAfter(this);
      $(this).remove();

      $.ajax({
        url: "/set_unread/" + nid[2] + "/" + sid
      }).done(function ( data ) {
        if( console && console.log ) {
          // console.log(data);
        }
      });

      // Set badge count
      $badgecount = $('a[rel=' + nid[2] + ']', window.parent.document) || $('a[rel=' + nid[2] + ']');
      if($badgecount.find('.badge').length > 0) {
        $badgecount.find('.badge').text($('.view_link .label').length);
      } else {
        $badgecount.append('<span class="badge">1</span>');
      }

      // Set total count
      $('.no_unread').removeClass('no_unread');

      $count = $('.unread_msg .count', window.parent.document) || $('.unread_msg .count');
      var k = parseInt($count.text(), 10);
      if(typeof(k) == "number") {
        k++;
        $count.text(k);
      } else {
        $count.text();
      }
    });

    // 
    $('.sticky-table').on('click', '.label', function(e) {
      e.stopPropagation();
      var nid = $(this).prev('a').attr('href').split('/');
      var sid = $(this).parent().parent().addClass('wf_unread').find('.active').text();
      $('<span class="mark">mark unread</span>').insertAfter(this);
      $(this).parent().parent().removeClass('wf_unread');
      $(this).remove();

      $.ajax({
        url: "/set_read/" + nid[2] + "/" + sid
      }).done(function ( data ) {
        if( console && console.log ) {
          // console.log(data);
        }
      });

      // Set badge count
      $badgecount = $('a[rel=' + nid[2] + ']', window.parent.document) || $('a[rel=' + nid[2] + ']');
      if($badgecount.find('.badge').length > 0) {
        $badgecount.find('.badge').text($('.view_link .label').length);
      } else {
        $badgecount.append('<span class="badge">1</span>');
      }

      // Set total count
      $count = $('.unread_msg .count', window.parent.document) || $('.unread_msg .count');
      var k = parseInt($count.text(), 10);
      if(typeof(k) == "number") {
        k--;
        $count.text(k);
      } else {
        $count.text();
      }

    });

    $('.webform_improved tr').find('td:eq(0), td:eq(1), td:eq(2), td:eq(3)').click(function() {
      var overlay = '/';
      var url = '';
      if($('.overlay_open').length > 0) {
        overlay = '#overlay=';
        url = window.top.location.href;
        url = url.split('#');
        url = url[0];
      }
      window.top.location.href = url + overlay + $(this).parent().find('.view_link a').attr('href').substr(1);
    });

  }

  var $menu = $("#toolbar .unread_forms", window.parent.document) || $("#toolbar .unread_forms");
  var $count = $("#toolbar .count", window.parent.document) || $("#toolbar .count");
  $t = $("#toolbar-user", window.parent.document) || $("#toolbar-user");

  $menu.hammer({
    'tap': true,
    'tap_double': false
  })
  .bind("swipe", function(ev) {
    ev.stopPropagation();
    ev.preventDefault();
    $menu.slideUp();
  });

  $count.hammer({
    'tap': true,
    'tap_double': false,
    'tap_max_interval': 1000
  })
  .bind("tap", function(ev) {
    ev.preventDefault();
    ev.stopPropagation();
    
    if($menu.hasClass('init')) {
      $menu.slideToggle();
    } else {
      $menu.hide();
      $menu.slideDown();
      $menu.addClass('init');
    }
  });

  if($('.page-node-webform-results').length > 0) {
    setTimeout(function() {
      setRead();
    }, 50);
  }
});

})(jQuery);