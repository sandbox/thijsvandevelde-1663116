<?php
$unreads = array();
$nodes = array();
$msgs = array();

//if(count($unread_forms) > 0) {
  foreach ($unread_forms as $key => $value) {
    $f = explode('|', $key);

    if(!in_array($value, $nodes)) {
      array_push($nodes, $value);
    }

    if(!in_array($f[1].'|'.$value, $msgs)) {
      array_push($msgs, $f[1].'|'.$value);
    }

    if(array_key_exists($f[0], $unreads)) {
      $unreads[$f[0]] = ((int)$unreads[$f[0]] + 1);
    } else {
      $unreads[$f[0]] = 1;
    }
  }

  $msgs = implode(',', $msgs);
//}
?>
    <div class="unread_msg">
<?php
$i = 0;
//if(count($unread_forms) > 0) {
  foreach ($unreads as $key => $value) {
      print '<a rel="' . $nodes[$i] . '" href="tests/' . $lang . 'node/' . $nodes[$i++] . '/webform-results">' . $key . '</a><span class="badge">' . $value . '</span>';
  }
//}
?>
    </div>
