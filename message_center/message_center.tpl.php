<?php
$unreads = array();
$nodes = array();
$msgs = array();

// Overlay module?
$overlay = "/";
if(module_exists("overlay")) {
  $overlay = "#overlay=";
}

// Total number of messages?
$total = 0;
$ids = "";

foreach ($read_forms as $key => $value) {
  if($value['count'] != 0) {
    $total += count(explode(',', $value['count']));
    $ids .= $value['count'] . ',';
  }
}
$ids = substr_replace($ids,"",-1);

$no_unread = '';
if($total == 0) {
  $no_unread = " no_unread";
}

if($type == "toolbar") {
?>
    <div class="unread_msg<?php echo $no_unread; ?>" rel="<?php echo $ids; ?>">

<?php if($total == 0) { ?>
      <img src="/<?php echo drupal_get_path('module', 'message_center');?>/mail.png" width="24" />
      <span class="count"><?php echo $total; ?></span>
<?php } else { ?>
      <span class="count"><?php echo $total; ?></span>
<?php } ?>

      <div class="unread_forms">
      <div>
<?php
$i = 0;
foreach ($read_forms as $key => $value) {
  $count = count(explode(',', $value['count']));
  print '<a rel="' . $key . '" data-sid="' . $value['count'] . '" href="' . $overlay . 'node/' . $key . '/webform-results"><span class="unread_item">' . $value['title'] . '</span>';
  if($value['count'] > 0) print '<span class="badge">' . $count . '</span>';
  print '</a>';
}
?>
      </div>
      </div>
    </div>
<?php } ?>